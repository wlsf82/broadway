# Broadway

Sample project based on codecademy's ruby on rails PRO course.

## Installation

Run `bundle install` to install the project dependencies.

## Starting the app

Run `rails server` to start the app locally.

### Accessing the app locally

After the rails server is started, access the following URL in your preferable web browser: http://localhost:3000.

You should see something like the below sample.

![Broadway Rails App](./broadway-app.png)